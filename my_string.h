#include <iostream>


struct strholder
{
  std::string str;
  int instancecount;
};

class my_string{
   // private:
   // strholder *ptr;
  //  void detach();

    public:
    strholder *ptr;
    void CopyOnWrite();

//constuctors/////////////
    my_string():ptr(new strholder){
        ptr->instancecount=1;
        ptr->str="";
    }

    my_string(const std::string &string):ptr(new strholder){
        ptr->instancecount=1;
        ptr->str=string;
    }


    my_string(const my_string& other_string):ptr(other_string.ptr){

        ++ptr->instancecount;
    }


    	char& operator[](size_t index)
	{
        CopyOnWrite();
		return ptr->str[index];
	}
    ////////////////////////////////////////
    my_string& operator=(const my_string& other_string){    
        if(this == &other_string || ptr == other_string.ptr ){
            return *this;
        }
        --ptr->instancecount;
        if(ptr->instancecount==0){
            delete ptr;
        }
        ptr = other_string.ptr;
        ++ptr->instancecount;
        return *this;
             
    }

        my_string& operator=(const std::string &string){    
       CopyOnWrite();

             
    }



    ~my_string(){
        --ptr->instancecount;
        if(ptr->instancecount==0)
        delete ptr;
    }

};




void my_string::CopyOnWrite()
{ 
  if(ptr->instancecount == 1) 
     return;
 
   strholder* temp=new strholder;
   temp->str = ptr->str; 
   temp->instancecount = 1;
   --ptr->instancecount; 
   ptr = temp; 
}











