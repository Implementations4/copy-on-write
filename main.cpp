#include <iostream>
#include <my_string.h>

/*Copy-on-wrtie*/
int main()
{
my_string s1("abc");
std::cout<<"adress_s1= "<<s1.ptr<<std::endl;
std::cout<<"ref_s1= "<<s1.ptr->instancecount<<std::endl;


my_string s2(s1);
std::cout<<"ref_s1= "<<s1.ptr->instancecount<<std::endl;
std::cout<<"adress_s2= "<<s2.ptr<<std::endl;
std::cout<<"ref_s2= "<<s2.ptr->instancecount<<std::endl;
s2[0]='x';
std::cout<<"str_s2= "<<s2.ptr->str<<std::endl;
std::cout<<"ref_s2= "<<s2.ptr->instancecount<<std::endl;
std::cout<<"adress_s2= "<<s2.ptr<<std::endl;
my_string s3(s2);
std::cout<<"str_s2= "<<s2.ptr->str<<std::endl;
std::cout<<"ref_s2= "<<s2.ptr->instancecount<<std::endl;
std::cout<<"adress_s2= "<<s2.ptr<<std::endl;
s3[0]='1';
std::cout<<"str_s2= "<<s2.ptr->str<<std::endl;
std::cout<<"ref_s2= "<<s2.ptr->instancecount<<std::endl;
std::cout<<"adress_s2= "<<s2.ptr<<std::endl;
std::cout<<"str_s3= "<<s3.ptr->str<<std::endl;
std::cout<<"ref_s3= "<<s3.ptr->instancecount<<std::endl;
std::cout<<"adress_s3= "<<s3.ptr<<std::endl;


return 0;
 
}

